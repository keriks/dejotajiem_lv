dejotajiem_lv
=============

Dejotājiem.lv

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django
.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
     :target: https://github.com/ambv/black
     :alt: Black code style


:License: MIT



Basic Commands
--------------

* Ieinstalējam `Python3.7`_
* Ieinstalējam `PyCharm professional`_
* Noklonējam repozitoriju ar ssh vai izmantojot https: ::

    git clone git@bitbucket.org:keriks/dejotajiem_lv.git
    git clone https://keriks@bitbucket.org/keriks/dejotajiem_lv.git


* PyCharm File izvēlnē izvēlamies Open... un atveram pirmā līmeņa dejotajiem_lv
* PyCharm settingos zem Project settings > Project interpreter uzspiežam uz zobratiņa un izvēlamies Add.. un New virtual environment

.. image:: https://dl.dropboxusercontent.com/s/kgfg3ju7gj2u0e3/Add%20virtual%20environment.png
    :alt: Pycharm settings

* Settingos Languages & Frameworks > Django atzīmējam Enable Dajngo support

.. image:: https://dl.dropboxusercontent.com/s/hhnixn5xx9jqdhz/Preferences%20%3E%20django.png
    :alt: Pycharm settings

* Terminālī palaižam: ::

    pip install -r requirements/local.txt
    python manage.py migrate
    python manage.py createsuperuser


.. image:: https://dl.dropboxusercontent.com/s/29frkbk43s2rxei/Terminal.png
    :alt: Pycharm settings

* Zem Run izvēlamies Edit configurations... > + > Django Server

.. image:: https://dl.dropboxusercontent.com/s/03y7qqgifl3zyfa/Run%20config.png
    :alt: Pycharm settings

* Spiežam zaļo Run trīsstūri un veram vaļā http://localhost:8000


.. _`PyCharm professional`: https://www.jetbrains.com/pycharm/download/
.. _`Python3.7`: https://www.python.org/downloads/


Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^

* To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

* To create an **superuser account**, use this command::

    $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.



Email Server
^^^^^^^^^^^^

In development, it is often nice to be able to see emails that are being sent from your application. If you choose to use `MailHog`_ when generating the project a local SMTP server with a web interface will be available.

#. `Download the latest MailHog release`_ for your OS.

#. Rename the build to ``MailHog``.

#. Copy the file to the project root.

#. Make it executable: ::

    $ chmod +x MailHog

#. Spin up another terminal window and start it there: ::

    ./MailHog

#. Check out `<http://127.0.0.1:8025/>`_ to see how it goes.

Now you have your own mail server running locally, ready to receive whatever you send it.

.. _`Download the latest MailHog release`: https://github.com/mailhog/MailHog/releases

.. _mailhog: https://github.com/mailhog/MailHog



Deployment
----------

The following details how to deploy this application.




