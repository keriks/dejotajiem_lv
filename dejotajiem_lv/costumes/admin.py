from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin

from dejotajiem_lv.costumes.models import Costume, CostumePart, CostumePartHandout


@admin.register(Costume)
class CostumeAdmin(ModelAdmin):
    list_display = ["title", "collective", "issue_date", ]
    search_fields = ["title", "collective", "issue_date", ]


@admin.register(CostumePart)
class CostumePartAdmin(ModelAdmin):
    list_display = ["name", "identifier", "costume", ]
    search_fields = ["name", "identifier", "costume", ]


@admin.register(CostumePartHandout)
class CostumePartHandoutAdmin(ModelAdmin):
    list_display = ["issued_to", "issued_by", "costume_part", "received_date"]
    search_fields = ["issued_to", "issued_by", "costume_part", "received_date"]
