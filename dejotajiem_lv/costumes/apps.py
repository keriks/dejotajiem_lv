from django.apps import AppConfig


class CostumesConfig(AppConfig):
    name = 'dejotajiem_lv.costumes'
