from django.db import models

from dejotajiem_lv.dancegroups.models import Collective
from dejotajiem_lv.mixins.model_mixins import TimestampMixin
from dejotajiem_lv.users.models import User


def male_costume_path(instance, filename):
    return _costume_image_upload_to(instance, filename, True)


def female_costume_path(instance, filename):
    return _costume_image_upload_to(instance, filename, False)


def _costume_image_upload_to(instance, filename, male: bool = True):
    return 'costumes/{}/{}/%Y-%m-%d_{}'.format(instance.collective_id, instance.title, filename)


class Costume(TimestampMixin, models.Model):
    issue_date = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    title = models.CharField(max_length=255)
    collective = models.ForeignKey(Collective, on_delete=models.CASCADE)
    male_image = models.ImageField(upload_to=male_costume_path)
    female_image = models.ImageField(upload_to=female_costume_path)

    def __str__(self):
        return f"{self.collective}-{self.title}"

    class Meta:
        verbose_name = "Tērps"
        verbose_name_plural = "Tērpi"


class CostumePart(TimestampMixin, models.Model):
    name = models.CharField(max_length=255)
    identifier = models.CharField(max_length=255)
    costume = models.ForeignKey(Costume, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name}-{self.costume}"

    class Meta:
        verbose_name = "Tērpa sastāvdaļa"
        verbose_name_plural = "Tērpa sastāvdaļas"


class CostumePartHandout(TimestampMixin, models.Model):
    modified = models.DateTimeField(auto_now=True)
    issued_to = models.ForeignKey(User, related_name='received_costume_part_set', on_delete=models.CASCADE)
    issued_by = models.ForeignKey(User, related_name='issued_costume_part_set', on_delete=models.CASCADE)
    costume_part = models.ForeignKey(CostumePart, on_delete=models.CASCADE)
    received_date = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f"{self.issued_to}-{self.costume_part}"

    class Meta:
        verbose_name = "Izsniegtā tērpa sastāvdaļa"
        verbose_name_plural = "Izsniegtās tērpa sastāvdaļas"
