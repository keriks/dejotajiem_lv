from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin

from dejotajiem_lv.dancegroups.forms import CollectiveForm, GroupForm
from dejotajiem_lv.dancegroups.models import Collective, Group, Application, Payment, Rehearsal, WeekDay, Role


@admin.register(Collective)
class CollectiveAdmin(ModelAdmin):
    form = CollectiveForm
    list_display = ["title", "leader", "address", "email", "phone"]
    search_fields = ["title", "leader"]


@admin.register(Group)
class GroupAdmin(ModelAdmin):
    list_display = ["title", "collective", ]
    search_fields = ["title", "collective"]


@admin.register(Application)
class GroupAdmin(ModelAdmin):
    list_display = ["collective", "user", "status"]
    search_fields = ["user", "collective"]


@admin.register(Payment)
class PaymentAdmin(ModelAdmin):
    list_display = ["collective", "payed_by", "registered_by", "date", "amount"]
    search_fields = ["collective", "payed_by", "registered_by"]


@admin.register(Rehearsal)
class RehearsalAdmin(ModelAdmin):
    list_display = ["collective_group", "time", "end_time", "event_dance", "created_by"]
    search_fields = ["collective_group", "time", "event_dance", "created_by"]


@admin.register(WeekDay)
class WeekDayAdmin(ModelAdmin):
    list_display = ["day", ]


@admin.register(Role)
class RoleAdmin(ModelAdmin):
    list_display = ["collective", ]
    search_fields = ["collective", ]
