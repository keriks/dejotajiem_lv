from django.apps import AppConfig


class DanceGroupConfig(AppConfig):
    name = 'dejotajiem_lv.dancegroups'
