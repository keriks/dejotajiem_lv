import django_filters
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Column, Layout, Row, Submit
from django import forms
from django.db.models import Q

from dejotajiem_lv.dancegroups.models import Collective, Group, Application
from dejotajiem_lv.mixins.form_mixins import RequestKwargModelFormMixin


class CollectiveForm(RequestKwargModelFormMixin, forms.ModelForm):
    class Meta:
        model = Collective
        fields = ['title', 'address', 'leader']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column(
                    'title', 'address', 'leader',
                    css_class='col-xs-12 col-sm-6 col-md-4'
                )
            ),
            Row(
                Column(
                    Submit('submit', 'Saglabāt'),
                    css_class='col-xs-12 col-sm-6 col-md-4'
                )
            )
        )


class CollectiveFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(lookup_expr="icontains")

    class Meta:
        model = Collective
        fields = ['leader', 'title']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.filters['title'].field.widget.attrs.update({'placeholder': 'Meklēt...'})

        self.form.helper = FormHelper()
        self.form.helper.form_method = 'get'
        self.form.helper.form_class = 'enter-submit'

        self.form.helper.layout = Layout(
            Row(
                Column(
                    'leader',
                    css_class='col-sm-2 col-xs-4',
                ),
                Column(
                    'title',
                    css_class='col-sm-2 col-xs-4',
                ),
                Column(
                    Submit('submit', 'Search', css_class='btn btn-primary pull-right'),
                    css_class='col-sm-8 col-xs-4',
                )
            )
        )


class GroupForm(RequestKwargModelFormMixin, forms.ModelForm):
    class Meta:
        model = Group
        fields = ['title', 'collective']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.user.collective_set.count() > 1:
            self.fields['collective'].choices = [(c.pk, c.title) for c in self.user.collective_set.all()]
        else:
            self.fields['collective'].widget = forms.HiddenInput()
            self.fields['collective'].initial = self.user.collective_set.get().pk

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column(
                    'title', 'collective',
                    css_class='col-xs-12 col-sm-6 col-md-4'
                )
            ),
            Row(
                Column(
                    Submit('submit', 'Saglabāt'),
                    css_class='col-xs-12 col-sm-6 col-md-4'
                )
            )
        )


class GroupFilter(django_filters.FilterSet):
    title = django_filters.CharFilter(lookup_expr="icontains")

    class Meta:
        model = Group
        fields = ['title', ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.filters['title'].field.widget.attrs.update({'placeholder': 'Meklēt...'})

        self.form.helper = FormHelper()
        self.form.helper.form_method = 'get'
        self.form.helper.form_class = 'enter-submit'

        self.form.helper.layout = Layout(
            Row(
                Column(
                    'title',
                    css_class='col-sm-2 col-xs-4',
                ),
                Column(
                    Submit('submit', 'Search', css_class='btn btn-primary pull-right'),
                    css_class='col-sm-8 col-xs-4',
                )
            )
        )


class ApplicationCreateForm(RequestKwargModelFormMixin, forms.ModelForm):

    class Meta:
        model = Application
        fields = ['collective', 'user', "motivation"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['collective'].choices = [
            (c.pk, c.title) for c in Collective.objects.exclude(
                Q(leader_id=self.user.pk), Q(pk__in=self.user.group_set.all().values("collective_id"))
            )
        ]
        self.fields['user'].widget = forms.HiddenInput()

        self.helper = FormHelper()
        self.helper.layout = Layout(
            Row(
                Column(
                    'user', 'collective',
                    css_class='col-xs-12 col-sm-6 col-md-4'
                )
            ),
            Row(
                Column(
                    'motivation',
                    css_class='col-xs-12 col-sm-6 col-md-4'
                )
            ),
            Row(
                Column(
                    Submit('submit', 'Pieteikties'),
                    css_class='col-xs-12 col-sm-6 col-md-4'
                )
            )
        )
