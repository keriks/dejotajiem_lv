import datetime
import uuid

from django.db import models
from django.utils.text import slugify
from model_utils import Choices

from dejotajiem_lv.mixins.model_mixins import TimestampMixin
from dejotajiem_lv.users.models import User


def collective_logo_upload_to(instance, filename):
    return 'collectives/{}/%Y-%m-%d_{}'.format(str(uuid.uuid4()), filename)


class Collective(TimestampMixin, models.Model):
    title = models.CharField(max_length=255, unique=True)
    slug = models.SlugField(null=True, blank=True, unique=True)
    leader = models.ForeignKey(User, on_delete=models.PROTECT)
    address = models.CharField(max_length=255)
    logo = models.ImageField(null=True, blank=True, upload_to=collective_logo_upload_to)
    description = models.TextField()
    homepage = models.URLField()
    email = models.EmailField()
    phone = models.CharField(max_length=16)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Kolektīvs"
        verbose_name_plural = "Kolektīvi"


class Group(TimestampMixin, models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    collective = models.ForeignKey(Collective, on_delete=models.CASCADE)
    members = models.ManyToManyField(User)
    slug = models.SlugField(null=True, blank=True, unique=True)

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        return super().save(*args, **kwargs)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Kolektīva grupa"
        verbose_name_plural = "Kolektīva grupas"


class Payment(TimestampMixin, models.Model):
    date = models.DateField(default=datetime.date.today)
    description = models.CharField(max_length=255)
    amount = models.DecimalField(max_digits=11, decimal_places=2)
    registered_by = models.ForeignKey(User, related_name='registered_payment_set', on_delete=models.PROTECT)
    payed_by = models.ForeignKey(User, related_name='payed_payment_set', on_delete=models.SET_NULL, null=True)
    collective = models.ForeignKey(Collective, on_delete=models.CASCADE)
    incoming = models.BooleanField(default=True)

    class Meta:
        verbose_name = "Maksājums"
        verbose_name_plural = "Maksājumi"


class Role(TimestampMixin, models.Model):
    collective = models.ForeignKey(Collective, on_delete=models.CASCADE, null=True, blank=True)
    users = models.ManyToManyField(User)

    class Meta:
        verbose_name = "Dejotājs kolektīvā"
        verbose_name_plural = "Dejotāji kolektīvā"


class Application(TimestampMixin, models.Model):
    STATUSES = Choices(
        (0, "in_progress", "Iesniegts"),
        (1, "submitted", "Apstiprināts"),
        (2, "rejected", "Noraidīts"),
    )

    collective = models.ForeignKey(Collective, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    status = models.PositiveSmallIntegerField(choices=STATUSES, default=STATUSES.in_progress)
    motivation = models.TextField()

    def __str__(self):
        return f"{self.collective}-{self.user}"

    class Meta:
        verbose_name = "Pieteikums"
        verbose_name_plural = "Pieteikumi"


class WeekDay(models.Model):
    day = models.CharField(max_length=8)

    def __str__(self):
        return f"{self.day}"

    class Meta:
        verbose_name = "Nedēļas diena"
        verbose_name_plural = "Nedēļas dienas"


class Rehearsal(TimestampMixin, models.Model):
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    days = models.ManyToManyField(WeekDay)
    time = models.TimeField()
    end_time = models.TimeField()

    start_date = models.DateField()
    end_date = models.DateField()

    collective_group = models.ForeignKey(Collective, on_delete=models.CASCADE, null=True, blank=True)
    event_dance = models.ForeignKey('events.EventDance', on_delete=models.CASCADE, null=True, blank=True)

    comment = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return f"{self.collective_group}-{self.event_dance}"

    class Meta:
        verbose_name = "Mēģinājums"
        verbose_name_plural = "Mēģinājumi"
