import django_tables2 as tables
from django_tables2 import A

from dejotajiem_lv.dancegroups.models import Collective, Application


class CollectiveTable(tables.Table):
    title = tables.LinkColumn("dancegroups:collective_detail", kwargs={"slug": A('slug')})

    class Meta:
        model = Collective
        attrs = {'class': 'table table-hover table-striped'}
        fields = ['title', 'address', 'leader']
        per_page = 20
        order_by = ('-title',)


class CollectiveGroupTable(tables.Table):
    title = tables.LinkColumn("dancegroups:group_detail", kwargs={"collective_slug": A('collective.slug'),
                                                                  "slug": A('slug')})

    class Meta:
        model = Collective
        attrs = {'class': 'table table-hover table-striped'}
        fields = ['title', 'address', 'leader']
        per_page = 20
        order_by = ('-title',)


class ApplicationTable(tables.Table):
    action = tables.TemplateColumn('dancegroups/application_actions.html')

    class Meta:
        model = Application
        attrs = {'class': 'table table-hover table-striped'}
        fields = ['created', 'user', 'action']
        per_page = 50
        order_by = ('created',)
