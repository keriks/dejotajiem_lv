from django.urls import path

from dejotajiem_lv.dancegroups.views import (
    CollectiveListView, CollectiveCreateView, CollectiveDetailView, CollectiveUpdateView,
    GroupCreateView, GroupListView, GroupUpdateView, GroupDetailView,
    ApplicationCreateView, ApplicationListView)

app_name = "dancegroups"
urlpatterns = [
    path("", view=CollectiveListView.as_view(), name="collective_list"),
    path("jauns/", view=CollectiveCreateView.as_view(), name="collective_create"),
    path("<slug:slug>/", view=CollectiveDetailView.as_view(), name="collective_detail"),
    path("<slug:slug>/labot/", view=CollectiveUpdateView.as_view(), name="collective_update"),

    path("<str:collective_slug>/jauns/", view=GroupCreateView.as_view(), name="group_create"),
    path("<str:collective_slug>/grupas/", view=GroupListView.as_view(), name='group_list'),
    path("<str:collective_slug>/grupa/<slug:slug>/", view=GroupDetailView.as_view(), name="group_detail"),
    path("<str:collective_slug>/grupa/<slug:slug>/labot/", view=GroupUpdateView.as_view(), name="group_update"),

    path("<str:collective_slug>/pieteikums/jauns/", view=ApplicationCreateView.as_view(), name="application_create"),
    path("<str:collective_slug>/pieteikums/", view=ApplicationListView.as_view(), name="application_detail"),
]
