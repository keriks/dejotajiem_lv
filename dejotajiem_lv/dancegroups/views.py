from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, UpdateView, DetailView

from dejotajiem_lv.dancegroups.forms import CollectiveForm, CollectiveFilter, GroupForm, GroupFilter, \
    ApplicationCreateForm
from dejotajiem_lv.dancegroups.models import Collective, Group, Application
from dejotajiem_lv.dancegroups.tables import CollectiveTable, CollectiveGroupTable
from dejotajiem_lv.mixins.view_mixins import BaseSecureView, BaseTableView, BaseView


class CollectiveCreateView(BaseSecureView, CreateView):
    permission_required = 'dancegroups.add_collective'
    form_class = CollectiveForm
    model = Collective
    success_url = reverse_lazy('dancegroups:collective_list')
    template_name = 'base_form.html'


class CollectiveUpdateView(BaseSecureView, UpdateView):
    permission_required = 'dancegroups.change_collective'
    form_class = CollectiveForm
    model = Collective
    template_name = 'base_form.html'

    def get_success_url(self):
        return reverse_lazy('dancegroups:collective_detail', kwargs={"pk": self.object.slug})


class CollectiveDetailView(DetailView):
    model = Collective


class CollectiveListView(BaseTableView):
    model = Collective
    template_name = 'base_table.html'
    table_class = CollectiveTable
    filter_class = CollectiveFilter
    view_actions = [('Jauns', reverse_lazy("dancegroups:collective_create"))]


class GroupCreateView(BaseSecureView, CreateView):
    permission_required = 'dancegroups.add_collective'
    form_class = GroupForm
    model = Group
    template_name = 'base_form.html'

    def get_success_url(self):
        return reverse_lazy('dancegroups:collective_detail', kwargs={"slug": self.kwargs.get('collective_slug')})

    def get_initial(self):
        initial = super().get_initial()
        collective_slug = self.kwargs.get('collective_slug')
        initial.update(collective=Collective.objects.get(slug=collective_slug))
        return initial


class GroupUpdateView(BaseSecureView, UpdateView):
    permission_required = 'dancegroups.change_collective'
    form_class = GroupForm
    model = Group
    template_name = 'base_form.html'

    def get_initial(self):
        initial = super().get_initial()
        initial.update(collective=Collective.objects.get(slug=self.kwargs.get('collective_slug')))
        return initial

    def get_success_url(self):
        return reverse_lazy('dancegroups:collective_detail', kwargs={"slug": self.kwargs.get('collective_slug')})


class GroupDetailView(DetailView):
    model = Group

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.select_related('collective')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(dancers=self.object.members.all())
        return context


class GroupListView(BaseTableView):
    model = Group
    template_name = 'base_table.html'
    table_class = CollectiveGroupTable
    filter_class = GroupFilter


class ApplicationCreateView(LoginRequiredMixin, CreateView):
    model = Application
    form_class = ApplicationCreateForm
    template_name = 'base_form.html'

    def get_success_url(self):
        return reverse_lazy("dancegroups:collective_list")

    def get_initial(self):
        initial = super().get_initial()
        initial.update(collective=Collective.objects.get(slug=self.kwargs.get('collective_slug')),
                       user=self.request.user)
        return initial

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(request=self.request)
        return kwargs


class ApplicationListView(BaseSecureView, BaseTableView):
    model = Application
    table_class = CollectiveGroupTable
    permission_required = 'dancegroups.change_application'
