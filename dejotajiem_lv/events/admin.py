from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin

from dejotajiem_lv.events.models import Event, Dance, EventDance, NewsPost


@admin.register(Event)
class EventAdmin(ModelAdmin):
    list_display = ["title", "date", "location", "kind", "organiser"]
    search_fields = ["title", "kind", "organiser"]


@admin.register(Dance)
class DanceAdmin(ModelAdmin):
    list_display = ["title", "music_author", "music_performer", "choreographer",]
    search_fields = ["title", "music_author", "music_performer", "choreographer",]


@admin.register(EventDance)
class EventDanceAdmin(ModelAdmin):
    list_display = ["event", "dance", ]
    search_fields = ["event", "dance", ]


@admin.register(NewsPost)
class NewsPostAdmin(ModelAdmin):
    list_display = ["event", "title", "created_by"]
    search_fields = ["event", "title", "created_by"]


