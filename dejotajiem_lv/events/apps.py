from django.apps import AppConfig


class EventsConfig(AppConfig):
    name = 'dejotajiem_lv.events'
