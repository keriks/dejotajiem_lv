from django.db import models
from model_utils import Choices

from dejotajiem_lv.costumes.models import Costume
from dejotajiem_lv.dancegroups.models import Collective
from dejotajiem_lv.mixins.model_mixins import TimestampMixin
from dejotajiem_lv.users.models import User


def dance_song_upload_to(instance, filename):
    return 'songs/{}/%Y-%m-%d_{}'.format(instance.user.id, filename)


def event_placard_upload_to(instance, filename):
    return 'event/{}/%Y-%m-%d_{}'.format(instance.id, filename)


class Dance(TimestampMixin, models.Model):
    title = models.CharField(max_length=255)
    music_author = models.CharField(max_length=255, null=True, blank=True)
    music_performer = models.CharField(max_length=255, null=True, blank=True)
    choreographer = models.CharField(max_length=255, null=True, blank=True)
    song = models.FileField(upload_to=dance_song_upload_to)

    def __str__(self):
        return f"{self.title}"

    class Meta:
        verbose_name = "Deja"
        verbose_name_plural = "Dejas"


class Event(TimestampMixin, models.Model):
    KIND = Choices(
        (0, 'private', 'Private'),
        (1, 'public', 'Public')
    )

    title = models.CharField(max_length=255)
    date = models.DateField()
    location = models.CharField(max_length=255)
    kind = models.SmallIntegerField(choices=KIND, default=KIND.public)
    placard = models.ImageField(upload_to=event_placard_upload_to, null=True, blank=True)
    info = models.TextField()
    organiser = models.ForeignKey(User, related_name='event_organiser_set', on_delete=models.PROTECT)
    organiser_helpers = models.ManyToManyField(User, related_name='event_helper_set')
    dances = models.ManyToManyField(Dance, through='EventDance', through_fields=('event', 'dance'))

    def __str__(self):
        return f"{self.title}"

    class Meta:
        verbose_name = "Pasākums"
        verbose_name_plural = "Pasākumi"


class EventDance(TimestampMixin, models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    dance = models.ForeignKey(Dance, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.event}-{self.dance}"

    class Meta:
        verbose_name = "Deja pasākumā"
        verbose_name_plural = "Dejas pasākumā"


class EventDanceDancers(TimestampMixin, models.Model):
    event_dance = models.ForeignKey(EventDance, on_delete=models.CASCADE)
    collective = models.ForeignKey(Collective, on_delete=models.CASCADE)
    dancer = models.ForeignKey(User, on_delete=models.CASCADE)
    costume = models.ForeignKey(Costume, on_delete=models.CASCADE)


class NewsPost(TimestampMixin, models.Model):
    created_by = models.ForeignKey(User, on_delete=models.PROTECT)
    title = models.CharField(max_length=255)
    body = models.TextField()
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.event}-{self.title}"

    class Meta:
        verbose_name = "Pasākuma ziņa"
        verbose_name_plural = "Pasākuma ziņas"
