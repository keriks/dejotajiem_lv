class RequestKwargModelFormMixin:
    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop("request", None)
        self.request_kwargs = kwargs.pop("request_kwargs", None)
        if self.request:
            self.user = self.request.user
        super().__init__(*args, **kwargs)
