from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.http import QueryDict
from django_tables2 import SingleTableView


class BaseView:

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update(request=self.request)
        return kwargs


class BaseSecureView(PermissionRequiredMixin, LoginRequiredMixin, BaseView):
    pass


class BaseTableView(BaseView, SingleTableView):
    filter_class = None
    table_class = None
    table_data = None
    request = None
    filter_initial = None

    @property
    def paginate_by(self):
        if self.table_class and not self.filter_class:
            return 25

    def get_queryset(self):
        queryset = super().get_queryset()

        if self.filter_class:
            initial = QueryDict(self.filter_initial, mutable=True)
            initial.update(self.request.GET)
            queryset = self.filter_class(initial, queryset=queryset, request=self.request)

        return queryset

    def get_table_data(self):
        """
        Overriding get_table_data function from django_tables2, because django_tables2 interferes with django_filter.
        Returns: Correct QuerySet

        """
        if self.table_data is not None:
            return self.table_data
        elif hasattr(self, 'object_list'):
            if hasattr(self.object_list, "qs"):
                return self.object_list.qs
            return self.object_list
