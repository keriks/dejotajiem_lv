from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

from dejotajiem_lv.users.forms import UserChangeForm, UserCreationForm
from dejotajiem_lv.users.models import Message

User = get_user_model()


@admin.register(User)
class UserAdmin(auth_admin.UserAdmin):

    form = UserChangeForm
    add_form = UserCreationForm
    fieldsets = auth_admin.UserAdmin.fieldsets
    list_display = ["username", "name", "is_superuser"]
    search_fields = ["first_name", 'last_name', 'username']


@admin.register(Message)
class MessagesAdmin(ModelAdmin):
    list_display = ["sender", "receiver", "title", "body", "is_read"]
    search_fields = ["sender", "receiver"]
