import uuid

from django.contrib.auth.models import AbstractUser
from django.db import models
from django.urls import reverse

from dejotajiem_lv.mixins.model_mixins import TimestampMixin


def profile_picture_upload_to(instance, filename):
    return 'users/{}/%Y-%m-%d_{}'.format(str(uuid.uuid4()), filename)


class User(AbstractUser):
    date_of_birth = models.DateField(null=True, blank=True)
    picture = models.ImageField(null=True, blank=True, upload_to=profile_picture_upload_to)
    phone = models.CharField(max_length=16)

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})

    def get_full_name(self):
        if self.first_name or self.last_name:
            full_name = '%s %s' % (self.first_name, self.last_name)
        else:
            full_name = self.username
        return full_name.strip()

    @property
    def name(self):
        return self.get_full_name()

    class Meta:
        verbose_name = "Lietotājs"
        verbose_name_plural = "Lietotāji"


class Message(TimestampMixin, models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sent_messages_set')
    receiver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='received_messages_set')
    title = models.CharField(max_length=255, null=True, blank=True)
    body = models.TextField()
    is_read = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Ziņojums"
        verbose_name_plural = "Ziņojumi"
